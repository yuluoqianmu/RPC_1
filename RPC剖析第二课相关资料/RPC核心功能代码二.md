




#### 超时丢弃



```java

/**
* 执行异步任务
* 超时时间
* 超过此时间，则打印任务在队列中的时间
* @param handler 任务handler
*/
public void run(Group group, IAsyncHandler handler) {
   AsyncTask task = new AsyncTask(taskTimeout, handler);
   balance(group, task);
}

/**
* 构造异步任务
* @param timeout 超时时间(单位：豪秒)
* @param handler 执行句柄
*/
 public AsyncTask(int timeout, IAsyncHandler handler) {
 		super();
 		if (timeout < 0) {
        timeout = 1000;
    }
    this.timeout = timeout;
    this.handler = handler;
    this.addTime = System.currentTimeMillis();
 }

/**
* 任务执行
**/
public void run() {
    while (!isStop) {
      	AsyncTask task = null;
      	try {
          	task = taskQueue.poll(1500, TimeUnit.MILLISECONDS);
          	if (null != task) {
              	//重点执行语句： 执行超时丢弃的任务队列
              	execTimeoutTask(task);
            }
        	} catch (InterruptedException ie) {
          	logger.error("has error !", ie);
        	} catch (Throwable ex) {
          	if (task != null) {
              	task.getHandler().exceptionCaught(ex);
            	}
        	}
    	}
}


/**
	* @param task
  * @throws Throwable
*/
private void execTimeoutTask(AsyncTask task) throws Throwable {
  	if ((System.currentTimeMillis() - task.getAddTime()) > task.getQtimeout()) {
      	task.getHandler().exceptionCaught(new TimeoutException(threadFactoryName + "async task timeout!"));
      	return;
    } else {
      	Object obj = task.getHandler().run();
      	task.getHandler().messageReceived(obj);
   	}
}
```





#### 优雅关闭



```java
/**
* 初始化监听信号
*/
public void init() {
  	signalRegistry.register();
}

/**
* 初始化信号
*/
public void register() {
	try {
    	if (StringUtils.isNotBlank(osName) {
        	Signal sig = new Signal("USR2");	
        	Signal.handle(sig, operateSignalHandler);
        	Signal sig2 = new Signal("STKFLT");
        	Signal.handle(sig2, breakHeartbeatSignal);
      } catch (Exception e) {
    	logger.error( "------------------signal register failed !----------------------" , e);
  		}
}

public class OperateSignal implements SignalHandler {
  
  	private static Logger logger = LoggerFactory.getLogger(OperateSignal.class);

    @Autowired
    private RpcContext rpcContext;
  
		@Override
    public void handle(Signal signalName) {
        logger.info("[ARCH_SCF_SERVER_signalReceived]{signal = {}, user = {}}", signalName.getName(), System.getenv("REALUSERNAME"));
        logger.info("server : {} current state is :{}", new Object[]{zzscfContext.getServiceName(), zzscfContext.getServerState()});
        //设置当前服务状态为重启
        rpcContext.setServerState(ServerStateType.Reboot);
    		logger.info("serviceName: " + serviceName + " tcp.port: " + port + " telnet.port: " + telPort + " receive signal " + signalName.getName() + ", Server will break heartbeat with srvmgr ");

    }
}
          
  


// RequestFilter
public void filter(RpcContext context) throws Exception {
  if (zzscfContext.getServerState() == ServerStateType.Reboot && (protocol.getPlatformType() == PlatformType.Java || protocol.getPlatformType() == PlatformType.PHP)) {
    	RpcResponse response = new RpcResponse();
    	ResetProtocol rp = new ResetProtocol();
    	rp.setMsg("This server is reboot!");
    	responseProtocol.setSdpEntity(rp);
    	responseProtocol.setSDPType(SDPType.Reset);
    	response.setResponseBuffer(responseProtocol.toBytes(false, null));
    	context.setRpcResponse(response);
    	context.setExecFilter(ExecFilterType.None);
    	context.setDoInvoke(false);
 		}
}



 
public Object innerInvoke(RpcContext context, MethodSignature methodSignature) throws Exception {
        requestFilter(context);
        logger.debug("------------------------------ begin request-----------------------------");
        Object response = null;
        RpcContext.setThreadLocal(context)

        //判断是否需要Invoke，在filter中被设置
        if (context.isDoInvoke()) {
            //反射调用实际处理方法逻辑
            ......
            ......
        }
       
        context.getResponseProtocol().setSdpEntity(response);
        responseFilter(context);
        RpcContext.clear();
        return context;
}
          
protected void requestFilter(RpcContext context) throws Exception {
        logger.debug("begin requestFilters");
        for (IFilter filter : requestFilters) {
						if (context.getExecFilter() == ExecFilterType.All || context.getExecFilter() == ExecFilterType.RequestOnly) {
        				filter.filter(context);
            }
        }
  	logger.debug("end requestFilters");
}


          

          
public Protocol request(Protocol requestProtocol) throws Exception {
  			// 判断服务节点状态
       	if(ServerState.Reboot = false || ServerState.Dead == state){
          	throw new RebootException();
        }
  			....
        //注册窗口事件
        socket.registerRec(requestProtocol.getSessionId());
  			//异步发送请求
  			socket.send(data);
  			....
          
        //接收数据
       	deserialize(responseProtocol);
  			//判断回包
        SDPType responseSdpType = responseProtocol.getSDPType();
  			if (SDPType == SDPType.Reset) {
          	//服务节点不可用
          	this.asReboot();
          	//向上抛出服务重启异常
      			throw new RebootExecption();
        }
}

     

```







#### 过载保护



```java
/**
	* 执行异步任务
*/
public void run(Group group, IAsyncHandler handler) {
        AsyncTask task = new AsyncTask(taskTimeout, handler, inQueueTime);
        balance(group, task);
}

public void balance(Group group, AsyncTask task) {
  		if (group == Group.HIGH) {
        	//如果没有配置highGroup自动负载到defaultGroup
      		balanceTask(highFactor.getAndIncrement(), groupHighWorkers, task);
    	} else if (group == Group.DEFAULT) {
      		balanceTask(defaultFactor.getAndIncrement(), defaultWorkers, task);
    	}
}

/**
	* 向队列中添加任务 
*/
public void balanceTask(int factor, AsyncWorker[] workers, AsyncTask task) {
        int idx = factor % workers.length;
        if (limitSize > 0) {
            workers[idx].addTask(task, limitSize, mode);
        } else {
          workers[idx].addTask(task);
        }
}


/**
* 向队列中添加任务
* @param task
* @param limitSize    限定worker队列的长度
* @param abortNewTask 是否抛弃新的任务 当队列的长度达到定长 true表示抛弃新来的任务 false表示抛弃老的任务
* @return true 表示添加task成功 false表示添加task失败
*/
void addTask(AsyncTask task, int limitSize, boolean abortNewTask) {
  	// FIXME 这里直接offer, 根据offer结果决定是否返回异常, 判断长度非线程安全
  	if (this.taskQueue.size() >= limitSize) {
      	if (abortNewTask) {
          	// 抛弃新来的任务
          	task.getHandler().exceptionCaught(new TimeoutException(threadFactoryName + " abort this task, because the queue is full!"));
        	} else {
          	elimintateOldTask(task);
        	}
    	} else {
      	this.taskQueue.offer(task);
    	}
}
```


